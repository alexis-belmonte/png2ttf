#!/usr/bin/env python3.11
#
# png2ttf, an utility to convert a bitmap character set to a TrueType font,
# with the help of Pillow and FontForge
# Under the MIT License
# 
# Copyright (c) 2023 Alexis Belmonte
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

import typing
import os
import argparse
from PIL import Image as pilimage
import fontforge
import re
import tempfile
import unicodedata

Range = tuple[int, int]

class BitmapFont:
    SCALING_FACTOR = 16

    FONT_WEIGHTS = {
        100: "Thin",
        200: "Ultra-Light",
        300: "Light",
        400: "Normal",
        500: "Medium",
        600: "Demi-Bold",
        700: "Bold",
        800: "Extra-Bold",
        900: "Heavy"
    }

    @classmethod
    def translate_weight(cls, font_weight: int) -> str:
        nearest_weight = 400

        for weight in cls.FONT_WEIGHTS.keys():
            difference = abs(weight - font_weight)
            if difference < weight - nearest_weight:
                nearest_weight = weight

        return cls.FONT_WEIGHTS[nearest_weight]

    def __init__(self, glyph_width: int, glyph_height: int): 
        self.glyph_width  = glyph_width
        self.glyph_height = glyph_height

        self.glyphs = {}

    def import_stream(self, bitmap_stream: typing.IO, codepoint_range: Range):
        if codepoint_range[0] > codepoint_range[1]:
            raise ValueError("Invalid glyph range specified")
        
        glyph_count_required = codepoint_range[1] - codepoint_range[0]

        with pilimage.open(bitmap_stream) as image:
            (bitmap_width, bitmap_height) = image.size

            glyph_rows = bitmap_width // self.glyph_width
            glyph_cols = bitmap_height // self.glyph_height
            actual_glyph_count = glyph_rows * glyph_cols

            if actual_glyph_count < glyph_count_required:
                raise ValueError(
                   ("Range of codepoints ({:08x} .. {:08x}) does not satisfy the number of available glyphs " +
                    "({:d}, {:d} rows x {:d} columns)") \
                        .format(*codepoint_range, actual_glyph_count, glyph_rows, glyph_cols)
                )

            for codepoint in range(codepoint_range[0], codepoint_range[1] + 1):
                index = codepoint - codepoint_range[0]

                x = index * self.glyph_width % bitmap_width
                y = (index // glyph_rows) * self.glyph_height

                self.glyphs[codepoint] = image \
                    .crop((x, y, x + self.glyph_width, y + self.glyph_height)) \
                    .convert("1") \
                    .resize((self.glyph_width * BitmapFont.SCALING_FACTOR, self.glyph_height * BitmapFont.SCALING_FACTOR),
                            resample=0)

    def import_file(self, bitmap_file: str, codepoint_range: Range):
        with open(bitmap_file, "rb") as stream:
            return self.import_stream(stream, codepoint_range)

    def export_stream(self, font_stream: typing.IO, font_name: str, font_family: str, font_comment: str, font_copyright: str, font_spacing: int, font_weight: int, font_whitespace_size: int, ascent: int, descent: int, variable_width: bool):
        truetype_font = fontforge.font()

        truetype_font.fontname    = re.sub(r"[\W_]+", "", font_name)
        truetype_font.familyname  = font_name
        truetype_font.fullname    = font_name
        truetype_font.comment     = font_comment
        truetype_font.copyright   = font_copyright
        truetype_font.weight      = BitmapFont.translate_weight(font_weight)
        truetype_font.os2_weight  = font_weight
        truetype_font.ascent      = ascent * BitmapFont.SCALING_FACTOR
        truetype_font.descent     = descent * BitmapFont.SCALING_FACTOR
        truetype_font.os2_version = -1

        truetype_glyph_set = []

        for codepoint, glyph in self.glyphs.items():
            truetype_glyph = truetype_font.createMappedChar(codepoint)

            with tempfile.NamedTemporaryFile(suffix=".bmp") as glyph_temp_file:
                glyph_temp_file.close()
                glyph.save(glyph_temp_file.name)

                truetype_glyph.importOutlines(glyph_temp_file.name)

                truetype_glyph.left_side_bearing  = font_spacing * BitmapFont.SCALING_FACTOR
                truetype_glyph.right_side_bearing = (self.glyph_width + font_spacing) * BitmapFont.SCALING_FACTOR
                truetype_glyph.transform([1, 0, 0, 1, -font_spacing * BitmapFont.SCALING_FACTOR, 0])

                # NamedTemporaryFile does not actually delete itself when exiting the with block.
                os.unlink(glyph_temp_file.name)

        truetype_font.selection.all()
        truetype_font.autoTrace()
        truetype_font.removeOverlap()
        
        if variable_width:
            truetype_font.autoWidth((font_spacing - 1) * BitmapFont.SCALING_FACTOR, minBearing=0)
            for truetype_glyph in truetype_font.glyphs():
                if unicodedata.category(chr(truetype_glyph.unicode)).startswith("Z"):
                    truetype_glyph.right_side_bearing = font_whitespace_size * BitmapFont.SCALING_FACTOR
                else:
                    # Correct FontForge's attempt at centering glyphs, which makes them blurry on 1:1 scale
                    truetype_glyph.transform([1, 0, 0, 1, -BitmapFont.SCALING_FACTOR / 2, 0])
                    truetype_glyph.right_side_bearing = int(truetype_glyph.right_side_bearing + BitmapFont.SCALING_FACTOR // 2)

        with tempfile.NamedTemporaryFile(suffix=".ttf") as font_file:
            font_file.close()
            truetype_font.generate(font_file.name)

            with open(font_file.name, "rb") as actual_font_file:
                font_stream.write(actual_font_file.read())

            # NamedTemporaryFile does not actually delete itself when exiting the with block.
            os.unlink(font_file.name)

class App:
    def __init__(self):
        self.arg_parser = argparse.ArgumentParser(
            description="Convert a bitmap font image to a TrueType font"
        )
        
        self.arg_parser.add_argument("file", type=argparse.FileType("rb"), help="source bitmap font image file to convert from")

        self.arg_parser.add_argument("-W", "--glyph-width",          type=int,                     required=True, help="width of one glyph character in pixels")
        self.arg_parser.add_argument("-H", "--glyph-height",         type=int,                     required=True, help="height of one glyph character in pixels")
        self.arg_parser.add_argument(      "--glyph-range-start",    type=int,                     default=0x00,  help="from which code point shall the glyphs correspond to")
        self.arg_parser.add_argument(      "--glyph-range-end",      type=int,                     default=0xff,  help="to which code point shall the glyphs correspond to")
        self.arg_parser.add_argument(      "--font-name",            type=str,                     default=None,  help="name of the TrueType font to specify")
        self.arg_parser.add_argument(      "--font-family",          type=str,                     default=None,  help="family of the TrueType font to specify")
        self.arg_parser.add_argument(      "--font-comment",         type=str,                     default="",    help="comment or description of the TrueType font to specify")
        self.arg_parser.add_argument(      "--font-copyright",       type=str,                     default="",    help="copyright string of the TrueType font to specify")
        self.arg_parser.add_argument(      "--font-weight",          type=int,                     default=400,   help="weight of the TrueType font to specify")
        self.arg_parser.add_argument(      "--font-spacing",         type=int,                     default=2,     help="spacing of each glyph in pixels used by the TrueType font")
        self.arg_parser.add_argument(      "--font-whitespace-size", type=int,                     default=None,  help="override size of whitespace characters with the specified amount of pixels")
        self.arg_parser.add_argument(      "--variable-width",       action="store_true",                         help="let FontForge determine the width and bearings of each glyph")
        self.arg_parser.add_argument("-o", "--output",               type=argparse.FileType("wb"),                help="where to output the resulting TrueType font")

    def run(self):
        args = self.arg_parser.parse_args()

        if args.font_name is None:
            args.font_name = os.path.splitext(args.output.name)[0]
        if args.font_family is None:
            args.font_family = args.font_name
        if args.font_whitespace_size is None:
            args.font_whitespace_size = args.glyph_width

        bitmap_font = BitmapFont(args.glyph_width, args.glyph_height)
        bitmap_font.import_stream(
            bitmap_stream=args.file,
            codepoint_range=(args.glyph_range_start, args.glyph_range_end)
        )
        bitmap_font.export_stream(
            font_stream=args.output,
            font_name=args.font_name,
            font_family=args.font_family,
            font_comment=args.font_comment,
            font_copyright=args.font_copyright,
            font_weight=args.font_weight,
            font_spacing=args.font_spacing,
            font_whitespace_size=args.font_whitespace_size,
            ascent=args.glyph_height,
            descent=0,
            variable_width=args.variable_width
        )

app = App()
app.run()
