# PNG2TTF

Utility that allows you to convert a bitmap character set to a TrueType font
with proper scaling that can be used system-wide.

## Requirements

To run this utility, you will need:

| Dependency      | Version required | Why                                    |
|-----------------|------------------|----------------------------------------|
| `python`        | `3.11+`          | To run this script                     |
| `python-pillow` | `10.1.0-2+`      | To manipulate the charset bitmap image |
| `fontforge`     | `20230101+`      | To produce a TrueType font             |

**Note** : Older versions of those dependencies may be installed, but there is
           no guarantee that it will work.

### ArchLinux

All of those requirements may be satisfied on ArchLinux by executing the
following command as `root`:

```
# pacman -S python python-pillow fontforge
```

### Other Distributions and Operating Systems

For other distributions and operating systems, similar packages are probably
available in the repositories, or on the Internet.

Make sure that you install the latest version of each dependency, for the best
experience.

Unless if there's a huge demand for it, I will not fix or retroport this
utility to older versions of the required dependencies, mainly because older
versions mean uglier, more error-prone, and generally less understandable code.

## Usage

Issue `./png2ttf.py --help` at the root of this repository to get a list of
available flags to pass.

If you want to generate a TrueType font from an 8x8 character set bitmap image,
which covers the full Latin-1 set codepoints, you can simply use the following
command, assuming that your bitmap image is stored as `my_font.png`:

```bash
./png2ttf.py -W 8 -H 8 -o my_font.ttf my_font.png
```

## Demo

Check out [`extra`](/extra) for examples of use case scenarios. You can use the
[`gen_fonts.sh`](/extra/gen_fonts.sh) script to generate the Datasaur Monospace
and Variable fonts.

Here's also a bunch of screenshots for eyecandy:

![](/images/cool_retro_term.png)

![](/images/gnome_font_viewer_monospace.png)

![](/images/gnome_font_viewer_variable.png)

## Contributing

Contributions to improve or extend the utility are welcome!

Make sure to be as close as the original programming style. I did not follow
the Python PEP style as I find it a bit quirky at places, but you can correct
this if you want.

Do not forget to add yourself to the list of authors at the top of the script,
right before the actual MIT license **and** the `license.txt` file.

