#!/usr/bin/bash

../png2ttf.py \
    -o hd44780.ttf \
    -W 6 -H 8 \
    --font-name "Datasaur Monospace" \
    --font-comment "Datasaur Monospace Font, imported from the HD44780 font" \
    --font-copyright "Under the Open Font License" \
    hd44780.png

../png2ttf.py \
    -o hd44780_variable.ttf \
    -W 6 -H 8 \
    --font-name "Datasaur Variable" \
    --font-comment "Datasaur Variable Font, imported from the HD44780 font" \
    --font-copyright "Under the Open Font License" \
    --font-whitespace-size 3 \
    --variable-width \
    hd44780.png
